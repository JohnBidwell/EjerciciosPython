
def contar_letras(oracion):
    d = {}
    oracion = oracion.lower()
    for letra in oracion:
        if letra not in d:
            d[letra] = 0
        d[letra] = d[letra] + 1
    del d[" "]
    return d

def contar_vocales(oracion):
    v = {}
    oracion = oracion.lower()
    vocales = ("a", "e", "i", "o", "u")
    for letra in oracion:
        for vocales in letra:
            v[letra] = v[letra] + 1
    return v

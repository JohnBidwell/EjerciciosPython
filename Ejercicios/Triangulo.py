a = float(raw_input("Ingrese a: "))
b = float(raw_input("Ingrese b: "))
c = float(raw_input("Ingrese c: "))

triangulovalido = True

while True:
    if a > b+c:
        print "No es un triangulo valido"
        triangulovalido = False
    if b > a+c:
        print "No es un triangulo valido"
        triangulovalido = False
    if c > a+b:
        print "No es un triangulo valido"
        triangulovalido = False
    break

while (triangulovalido==True):
    if a==b and a==c and b==c :
        print "El triangulo es equilatero"
    elif a==b or a==c or b==c :
        print "El triangulo es isosceles"
    else:
        print "El triangulo es escaleno"
    break

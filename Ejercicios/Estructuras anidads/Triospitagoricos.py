def son_pitagoricos(a, b, c):
    if a**2 + b**2 == c**2:
        return True
    return False

def pitagoricos(n):
    pitagoricos = []
    for d in range(1, n+1):
        for e in range(1, n+1):
            for f in range(1,n+1):
                if son_pitagoricos(d, e, f):
                    pitagoricos.append((d, e, f))
    return pitagoricos

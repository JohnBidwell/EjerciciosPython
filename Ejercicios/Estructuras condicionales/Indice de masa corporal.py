estatura = float(raw_input("Estatura(mts): "))
peso = float(raw_input("Peso(kgs): "))
edad = float(raw_input("Edad: "))

IMC = peso/(estatura**2)

if edad < 45:
    if IMC < 22.0:
        print "Riesgo bajo de sufrir enfermedades coronarias"
    else:
        print "Riesgo medio de sufrir enfermedades coronarias"
else:
    if IMC < 22.0:
        print "Riesgo medio de sufrir enfermedades coronarias"
    else:
        print "Riesgo alto de sufrir enfermedades coronarias"
    

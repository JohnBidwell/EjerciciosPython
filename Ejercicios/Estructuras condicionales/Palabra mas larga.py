palabra1 = raw_input("Palabra 1: ")
palabra2 = raw_input("Palabra 2: ")

if len(palabra1)>len(palabra2):
    diferencia= len(palabra1)-len(palabra2)
    print "La palabra", palabra1, "tiene", diferencia, "letras mas que", palabra2,"."
elif len(palabra1)==len(palabra2):
    print "Las 2 palabras tienen el mismo largo."
else:
    diferencia= len(palabra2)-len(palabra1)
    print "La palabra", palabra2, "tiene", diferencia, "letras mas que", palabra1,"."

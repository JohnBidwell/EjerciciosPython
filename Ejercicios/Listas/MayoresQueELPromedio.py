#!/usr/bin/env python
# -*- coding: utf-8 -*-

datos = int(raw_input("Cuantos datos ingresará: "))

numeros = []


for i in range(1, datos+1):
	numero = float(raw_input("Dato " + str(i)+ ": "))
	numeros.append(numero)

suma = sum(numeros)
promedio = suma / datos
mayoresque = 0

for i in numeros:
	if i > promedio:
		mayoresque += 1

print str(mayoresque) + " datos son mayores que el promedio"
#!/usr/bin/env python
# -*- coding: utf-8 -*-

def mayores_que(x, valores):
	mayores = 0
	for i in valores:
		if i > x:
			mayores += 1
	return mayores
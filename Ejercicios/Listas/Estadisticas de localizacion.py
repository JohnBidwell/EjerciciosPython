#!/usr/bin/env python
# -*- coding: utf-8 -*-

def media_aritmetica(datos):
	suma = float(sum(datos))
	promedio = suma/len(datos)
	return promedio

def media_armonica(datos):
	suma = 0.0
	for i in datos:
		suma += 1.0/i
	return len(datos)/suma

def mediana(datos):
	mitad = len(datos)/2
	lista = sorted(datos)
	if len(lista)%2==0:
		mediana = (lista[mitad -1] +lista[mitad])/2
	else:
		mediana = lista[mitad]
	return mediana

def modas(datos):
	
	modas = [ ]
	veces1 = 0
	for i in datos:
		veces = 0
		for j in datos:
			if i == j:
				veces += 1
			veces1 = max(veces1, veces)
	for i in datos:
		veces2= 0
		for j in datos:
			if i==j:
				veces2 += 1
			if veces2== veces1 and i not in modas:
				modas.append(i)

	return modas


def main():
	datos = int(raw_input("Cuantos datos ingresará: "))
	lista = []
	for i in range(1, datos+1):
		dato = float(raw_input("Valor" + str(i) + ": "))
		lista.append(dato)
	print "La media aritmética es " + str(media_aritmetica(lista))
	if 0 not in lista:
		print "La media armónica es " + str(media_armonica(lista))
	print "La mediana es " + str(mediana(lista))
	print "La(s) modas son " + str(modas(lista))

if __name__ == '__main__':
    main()



def romano_a_arabigo(Romano):
    romanos = {
    "M" : 1000,
    "D" : 500,
    "C" : 100,
    "L" : 50,
    "X" : 10,
    "V" : 5,
    "I" : 1
    }

    arabigo = 0
    
    for i in range(len(Romano)):
        numactual = romanos[Romano[i]]
        if len(Romano)-1 > i:
            numsiguiente= romanos[Romano[i+1]]
            if numactual >= numsiguiente:
                arabigo += numactual
            else:
                arabigo -= numactual
        else:
            arabigo += numactual
    return arabigo    
            
        

from random import randrange

intentos = 1
mayor = 101
menor = 0
respuesta = "none"

while respuesta != "=":
    n = randrange(menor, mayor)
    print "Intento", str(intentos)+": ", n
    respuesta = raw_input("")
    if respuesta == "=":
        break
    if respuesta == ">" :
        menor = n
    elif respuesta == "<":
        mayor = n
    intentos+=1

print "Adivine en", intentos, "intentos :D"

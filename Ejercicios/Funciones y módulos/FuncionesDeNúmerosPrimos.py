#!/usr/bin/env python
# -*- coding: utf-8 -*-

def es_divisible(n,d):
	if n%d == 0:
		return True
	return False

def es_primo(n):
	if n==1:
		return False
	for d in range(2, n):
		if es_divisible(n, d) == True:
			return False
	return True
			
def i_esimo_primo(i):
	cont = 0
	n = 1
	while cont<i:
		n += 1
		if es_primo(n) == True:
			cont +=1
	return n

def primeros_primos(m):
	cont = 0
	primos = []
	n = 1
	while cont < m:
		n+=1
		if es_primo(n) == True:
			primos.append(n)
			cont += 1
	return primos

def primos_hasta(m):
	primos = []
	n = 1
	while n <= m:
		n+=1
		if es_primo(n) == True:
			primos.append(n)
	return primos






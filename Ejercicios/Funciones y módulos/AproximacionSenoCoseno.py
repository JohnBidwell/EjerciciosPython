def factorial_reciproco(n):
	factorial = 1
	for i in range(1, n+1):
		factorial *= i
	return 1/factorial

def signo(n):
	if n%2 == 0:
		return 1
	return (-1)

def seno_aprox(x, m):
	seno = 0.0
	for i in range(m):
		seno += (x**(2*i+1)) * signo(i) * factorial_reciproco(2*i+1)
	return seno



def coseno_aprox(x, m):
	coseno = 0.0
	for i in range(m):
				coseno += (x**(2*i)) * signo(i) * factorial_reciproco(2*i)
	return coseno


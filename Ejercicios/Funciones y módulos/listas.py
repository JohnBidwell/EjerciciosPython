def promedio(l):
	suma = 0.0
	for i in l:
		suma += i
	return suma/len(l)

def cuadrados(l):
	cuadrados = []
	for i in l:
		cuadrados.append(i**2)
	return cuadrados

def mas_largo(palabras):
	mas_larga = " "
	for i in palabras:
		if len(i)>len(mas_larga):
			mas_larga = i
	return mas_larga

